# AppleDeviceIdentifier

This package is inspired by [UIDeviceIdentifier](https://github.com/squarefrog/UIDeviceIdentifier) by [squarefrog](https://github.com/squarefrog/), but re-written in Swift and packaged using [Swift Package Manager](https://swift.org/package-manager/).

## Usage

```
if let appleDevice = AppleDevice.instance {
    print("modelName: \(appleDevice.modelName)") // "iPhone XR"
    print("identifier: \(appleDevice.identifier)") // "iPhone11,8"
}
```

## License

AppleDeviceIdentifier is available under the MIT license. See the LICENSE file for more info.

